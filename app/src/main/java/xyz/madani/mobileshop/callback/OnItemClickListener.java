package xyz.madani.mobileshop.callback;

/**
 * Created by shaladin on 12/23/17.
 */

public interface OnItemClickListener {
    void onClick(Object object, int position);
}
