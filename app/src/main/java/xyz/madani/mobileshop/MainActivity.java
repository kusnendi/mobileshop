package xyz.madani.mobileshop;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import xyz.madani.mobileshop.ui.fragment.AccountFragment;
import xyz.madani.mobileshop.ui.fragment.CartFragment;
import xyz.madani.mobileshop.ui.fragment.CategoryFragment;
import xyz.madani.mobileshop.ui.fragment.HomeFragment;
import xyz.madani.mobileshop.ui.fragment.transaction.TransactionFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppBarLayout appBar = (AppBarLayout) findViewById(R.id.appbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            appBar.setElevation(0);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Remove ActionBar Shadow
        getSupportActionBar().setElevation(0);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_content, new HomeFragment());
        transaction.commit();

        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(onTabSelectListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem optSearch = menu.findItem(R.id.optSearch);
        optSearch.setVisible(false);

//        MenuItem menuItem = menu.findItem(R.id.optNotify);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
//        searchView.setOnQueryTextListener(this);

        return super.onPrepareOptionsMenu(menu);
    }

    private OnTabSelectListener onTabSelectListener = new OnTabSelectListener() {
        @Override
        public void onTabSelected(@IdRes int tabId) {
            Fragment fragment = null;
            switch (tabId) {
                case R.id.menu_account:
                    fragment = new AccountFragment();
//                    navigate(fragment);
                    break;
                case R.id.menu_cart:
                    fragment = new CartFragment();
//                    Intent cart = new Intent(MainActivity.this, SimpleActivity.class);
//                    cart.putExtra("action", 10);
//                    startActivity(cart);
                    break;
                case R.id.menu_transaction:
                    fragment = new TransactionFragment();
                    break;
                case R.id.menu_category:
                    fragment = new CategoryFragment();
                    break;
                default:
                    fragment = new HomeFragment();
                    break;
            }

            navigate(fragment);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.optNotify) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void navigate(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        transaction.replace(R.id.main_content, fragment);
        transaction.commit();
    }
}
