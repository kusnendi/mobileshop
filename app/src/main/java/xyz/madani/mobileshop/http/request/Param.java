package xyz.madani.mobileshop.http.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shaladin on 12/26/17.
 */

public class Param {
    @SerializedName("current_page")
    @Expose
    private int current;
    private int next;
    private int prev;
    @SerializedName("last_page")
    @Expose
    private int last;
    @SerializedName("per_page")
    @Expose
    private int offset;
    @SerializedName("total")
    @Expose
    private int total;
    @SerializedName("path")
    @Expose
    private String path;

    public Param(int current, int last, int offset, int total) {
        this.current = current;
        this.last = last;
        this.offset = offset;
        this.total = total;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }

    public int getPrev() {
        return prev;
    }

    public void setPrev(int prev) {
        this.prev = prev;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
