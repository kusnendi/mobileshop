package xyz.madani.mobileshop.http.service;

import xyz.madani.mobileshop.http.RetrofitClient;

/**
 * Created by shaladin on 12/26/17.
 */

public class WebAPI {
    //properties
    public static final String baseUrl = "http://10.42.0.1:8000/api/";
//    public static final String baseUrl = "http://rndres-svr.ad-ins.com:8089/am-api/";
    public static final String product = "products/";
    public static final String lists   = "lists/";
    public static final String user    = "users/";
    public static final String cart    = "cart";
    public static final String order   = "order";

    //ProductService
    public static ProductService productService() {
        return RetrofitClient.getClient(baseUrl)
                .create(ProductService.class);
    }

    //CollectionService
    public static CollectionService collectionService() {
        return RetrofitClient.getClient(baseUrl.concat(lists))
                .create(CollectionService.class);
    }

    //CartService
    public static CartService cartService() {
        return RetrofitClient.getClient(baseUrl.concat(user))
                .create(CartService.class);
    }
}
