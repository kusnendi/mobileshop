package xyz.madani.mobileshop.http.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import xyz.madani.mobileshop.http.response.CartResponse;

/**
 * Created by shaladin on 12/26/17.
 */

public interface CartService {

    @GET("cart")
    Call<ResponseBody> rawCart(@Header("X-Token") String token);

    @GET("cart")
    Call<CartResponse> cart(@Header("X-Token") String token);
}
