package xyz.madani.mobileshop.http.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import xyz.madani.mobileshop.http.request.Param;
import xyz.madani.mobileshop.model.Product;

/**
 * Created by shaladin on 12/26/17.
 */

public class ProductResponse extends Param {
    @SerializedName("data")
    @Expose
    private List<Product> data;

    public ProductResponse(int current, int last, int offset, int total) {
        super(current, last, offset, total);
    }

    public List<Product> getData() {
        return data;
    }

    public void setData(List<Product> data) {
        this.data = data;
    }
}
