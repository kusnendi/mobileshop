package xyz.madani.mobileshop.http.service;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import xyz.madani.mobileshop.http.response.ProductResponse;
import xyz.madani.mobileshop.model.Product;

/**
 * Created by shaladin on 12/26/17.
 */

public interface ProductService {
    @GET("products")
    Call<Product[]> top();

    @GET("products")
    Call<ProductResponse> filter(@QueryMap Map<String, String> filters);

//    @GET("/")
//    Call<ResponseBody> filter();
}
