package xyz.madani.mobileshop.http.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import xyz.madani.mobileshop.model.Category;

/**
 * Created by shaladin on 12/26/17.
 */

public interface CollectionService {

    @GET("categories")
    Call<Category[]> categories(@Query("keyword") String keyword);
}
