
package xyz.madani.mobileshop.http.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import xyz.madani.mobileshop.http.common.CartDiscount;
import xyz.madani.mobileshop.http.common.Item;

public class CartResponse {

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("total_price")
    @Expose
    private Double totalPrice;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("discounts")
    @Expose
    private List<CartDiscount> discounts = null;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<CartDiscount> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<CartDiscount> discounts) {
        this.discounts = discounts;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
