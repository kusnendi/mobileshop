package xyz.madani.mobileshop.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.callback.OnItemClickListener;
import xyz.madani.mobileshop.model.Menu;
import xyz.madani.mobileshop.model.Payment;

/**
 * Created by developer on 11/12/17.
 */

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.MenuViewHolder> {
    private Context context;
    private List<Payment> payments;
    private SparseBooleanArray selectedIds;
    private OnItemClickListener onItemClickListener;

    public PaymentAdapter(Context context, List<Payment> payments) {
        this.context    = context;
        this.payments   = payments;
        selectedIds     = new SparseBooleanArray();
//        this.onItemClickListener = listener;
    }

    public PaymentAdapter(Context context, List<Payment> payments, OnItemClickListener listener) {
        this.context    = context;
        this.payments      = payments;
        this.onItemClickListener = listener;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MenuViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public int getItemCount() {
        return (payments != null) ? payments.size() : 0;
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, final int position) {
        final Payment payment = payments.get(position);

        if (position == 0) holder.separator.setVisibility(View.GONE);
        holder.title.setText(payment.getName());
        holder.icon.setImageResource(selectedIds.get(position) ? R.drawable.ic_radio_checked : R.drawable.ic_radio_unchecked);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                onItemClickListener.OnItemClickListener(menu, position);
                toggleSelection(position);
            }
        });
    }

    //Toggle selection method
    public void toggleSelection(int position){
        selectView(position, !selectedIds.get(position));
    }

    //Remove selected selections
    public void removeSelection() {
        selectedIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    //Put or Delete selected position to SparseBooleanArray
    public void selectView(int position, boolean value) {
        if(value) {
            removeSelection();
            selectedIds.put(position, value);
        } else {
            selectedIds.delete(position);
        }

        notifyDataSetChanged();
    }

    public static class MenuViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView icon;
        View separator;

        public MenuViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_simple, parent, false));

            title = itemView.findViewById(R.id.title);
            icon  = itemView.findViewById(R.id.icon);
            separator = itemView.findViewById(R.id.separator);
        }
    }
}
