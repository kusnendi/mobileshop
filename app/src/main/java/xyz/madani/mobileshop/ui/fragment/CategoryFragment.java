package xyz.madani.mobileshop.ui.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.SimpleActivity;
import xyz.madani.mobileshop.callback.OnItemClickListener;
import xyz.madani.mobileshop.common.Global;
import xyz.madani.mobileshop.http.service.CollectionService;
import xyz.madani.mobileshop.http.service.WebAPI;
import xyz.madani.mobileshop.model.Category;
import xyz.madani.mobileshop.ui.adapter.CategoryAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CategoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryFragment extends Fragment implements OnItemClickListener, TextView.OnEditorActionListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String query;
    private List<Category> all;
    private List<Category> categories;
    private CategoryAdapter categoryAdapter;

    TextView search;
    RecyclerView lists;


    public CategoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CategoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CategoryFragment newInstance(String param1, String param2) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

//        genData();
        categories = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        search= view.findViewById(R.id.search);
        search.setOnEditorActionListener(this);

        lists = view.findViewById(R.id.lists);
        LinearLayoutManager lmCategory = new LinearLayoutManager(getContext());
        lists.setHasFixedSize(true);
        lists.setLayoutManager(lmCategory);

        categoryAdapter = new CategoryAdapter(getContext(), categories, 20, this);
        lists.setAdapter(categoryAdapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 0 && all != null) {
                    categories.clear();
                    categories.addAll(all);
                    categoryAdapter.notifyDataSetChanged();

                    hideKeyboard(search.getWindowToken());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        loadData();
    }

    private void genData() {
        categories = new ArrayList<>();

        for (int i = 1; i<= 5; i++) {
            categories.add(new Category("Kategori " + i, R.drawable.ic_favorite));
        }
    }

    private void loadData() {
        //Load Categories Data
        getCategory(null);
    }

    @Override
    public void onClick(Object object, int position) {
        Category category = (Category) object;
        //Check Parent hasResult
        if (SimpleActivity.hasResult) {
            //Prepare Data
            Bundle bundle   = new Bundle();
            bundle.putInt("dataType", SimpleActivity.LIST_CATEGORY);
            bundle.putSerializable("result", category);
            Message message = new Message();
            message.setData(bundle);
            SimpleActivity.resultHandler.sendMessage(message);
        } else {
            Intent intent = new Intent(getContext(), SimpleActivity.class);
            intent.putExtra(Global.action, SimpleActivity.BROWSE);
            intent.putExtra("data", category);
            startActivity(intent);
        }
    }

    private void getCategory(String keyword) {
        CollectionService listService = WebAPI.collectionService();
        listService.categories(keyword).enqueue(new Callback<Category[]>() {
            @Override
            public void onResponse(Call<Category[]> call, Response<Category[]> response) {
                int code = response.code();

                switch (code) {
                    case 200:
                        Collections.addAll(categories, response.body());
                        categoryAdapter.notifyDataSetChanged();
                        break;
                }
            }

            @Override
            public void onFailure(Call<Category[]> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            hideKeyboard(search.getWindowToken());

            if (all == null) {
                all = new ArrayList<>();
                all.addAll(categories);
            }

            categories.clear();
            categoryAdapter.notifyDataSetChanged();

            query = v.getText().toString();
            getCategory(query);
        }
        return false;
    }

    private void hideKeyboard(IBinder windowToken) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(windowToken, 0);
    }
}
