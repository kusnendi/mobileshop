package xyz.madani.mobileshop.ui.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.SimpleActivity;
import xyz.madani.mobileshop.common.Global;
import xyz.madani.mobileshop.http.common.Item;
import xyz.madani.mobileshop.http.response.CartResponse;
import xyz.madani.mobileshop.http.service.CartService;
import xyz.madani.mobileshop.http.service.WebAPI;
import xyz.madani.mobileshop.model.Product;
import xyz.madani.mobileshop.ui.adapter.CartAdapter;
import xyz.madani.mobileshop.ui.custom.DividerItemDecoration;
import xyz.madani.mobileshop.ui.fragment.checkout.ShipAddressFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CartResponse dataCart;
    private CartAdapter cartAdapter;
    private List<Product> products;
    private List<Item> items;

    View view;
    TextView subtotal;
    RecyclerView vCarts;
    Button btnBuy;

    public CartFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CartFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        items = new ArrayList<>();
//        loadCart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cart, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.view   = view;
        vCarts      = view.findViewById(R.id.lists);
        btnBuy      = view.findViewById(R.id.btnBuy);
        subtotal    = view.findViewById(R.id.subtotal);

        LinearLayoutManager lmCarts = new LinearLayoutManager(getContext());
        vCarts.setLayoutManager(lmCarts);
        vCarts.setHasFixedSize(true);
        vCarts.setNestedScrollingEnabled(false);

        cartAdapter = new CartAdapter(getContext(), items);
        vCarts.setAdapter(cartAdapter);

        RecyclerView.ItemDecoration separator = new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL_LIST);
        vCarts.addItemDecoration(separator);

        btnBuy.setOnClickListener(this);

        loadCart();
    }

    private List<Product> dummyCarts() {
        List<Product> products = new ArrayList<>();

        for (int i = 1; i<=2; i++) {
            products.add(new Product(getString(R.string.dummy_product), 8500000, R.drawable.iphone_x));
        }

        return products;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btnBuy:
                gotoCheckout();
                break;
        }
    }

    private void gotoCheckout() {
        Intent checkout = new Intent(getContext(), SimpleActivity.class);
        checkout.putExtra("action", 40);
        startActivity(checkout);
    }

    private void loadCart() {
        CartService cartService = WebAPI.cartService();
        cartService.cart(Global.token).enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                int code = response.code();

                switch (code) {
                    case 200:
                        dataCart = response.body();
                        items.addAll(dataCart.getItems());
//                        System.out.println(items.size());

                        cartAdapter.notifyDataSetChanged();
//                        String displayPrice = String.format(Locale.US, "%,.0f", dataCart.getTotalPrice());
//                        subtotal.setText(getString(R.string.fmt_price, displayPrice));
                        view.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                String displayPrice = String.format(Locale.US, "%,.0f", dataCart.getTotalPrice());
                                subtotal.setText(getString(R.string.fmt_price, displayPrice));
                            }
                        }, 1000);
                        break;
                }
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });

//        cartService.rawCart(Global.token).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                int code = response.code();
//
//                if (code == 200) {
//                    try {
//                        System.out.println(response.body().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                t.printStackTrace();
//            }
//        });
    }
}
