package xyz.madani.mobileshop.ui.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.madani.mobileshop.SimpleActivity;
import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.callback.OnItemClickListener;
import xyz.madani.mobileshop.http.response.ProductResponse;
import xyz.madani.mobileshop.http.service.CollectionService;
import xyz.madani.mobileshop.http.service.ProductService;
import xyz.madani.mobileshop.http.service.WebAPI;
import xyz.madani.mobileshop.model.Banner;
import xyz.madani.mobileshop.model.Category;
import xyz.madani.mobileshop.model.Product;
import xyz.madani.mobileshop.ui.adapter.CategoryAdapter;
import xyz.madani.mobileshop.ui.adapter.ProductAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context context;
    private List<Banner> banners;
    private List<Category> categories;
    private List<Product> populars;
    private CategoryAdapter categoryAdapter;
    private ProductAdapter popularAdapter;

    SliderLayout slider;
    View sectionCategory, sectionPopular;
    RecyclerView categoryGroup, popularGroup;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

//        topProduct();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        categories  = new ArrayList<>();
        populars    = new ArrayList<>();

        dummyBanner();
//        dummyCategory();
//        dummyPopular();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        slider = view.findViewById(R.id.slider);
        sectionCategory = view.findViewById(R.id.sectionCategory);
        sectionPopular  = view.findViewById(R.id.sectionPopular);

        for (Banner banner : banners) {
            TextSliderView sliderItem = new TextSliderView(getContext());
            sliderItem.image(banner.getResourceId()).setScaleType(BaseSliderView.ScaleType.Fit);
            sliderItem.bundle(new Bundle());
            sliderItem.getBundle().putString("extra", banner.getName());

            slider.addSlider(sliderItem);
        }

        TextView headCategory = sectionCategory.findViewById(R.id.title);
        headCategory.setText("Kategori");

        categoryGroup = sectionCategory.findViewById(R.id.recycler);
        GridLayoutManager lmCategory = new GridLayoutManager(getContext(), 2, GridLayoutManager.HORIZONTAL, false);
        categoryGroup.setLayoutManager(lmCategory);
        categoryGroup.setHasFixedSize(true);
        categoryGroup.setNestedScrollingEnabled(false);

        categoryAdapter = new CategoryAdapter(getContext(), categories, 10, this);
        categoryGroup.setAdapter(categoryAdapter);

        TextView headPopular = sectionPopular.findViewById(R.id.title);
        headPopular.setText("Popular");

        popularGroup = sectionPopular.findViewById(R.id.recycler);
        GridLayoutManager lmPopular = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        popularGroup.setLayoutManager(lmPopular);
        popularGroup.setHasFixedSize(true);
        popularGroup.setNestedScrollingEnabled(false);

        popularAdapter = new ProductAdapter(getContext(), populars, 10);
        popularGroup.setAdapter(popularAdapter);

        loadData();
    }

    private void loadData() {
        //Get Initialize Categories
        getCategory();
        //Get Popular Products
        popularProduct();
    }

    private void dummyBanner() {
        banners = new ArrayList<>();

        banners.add(new Banner("Banner 1", R.drawable.promo_1));
        banners.add(new Banner("Banner 2", R.drawable.promo_2));
        banners.add(new Banner("Banner 3", R.drawable.promo_3));
        banners.add(new Banner("Banner 4", R.drawable.promo_4));
        banners.add(new Banner("Banner 5", R.drawable.promo_5));
        banners.add(new Banner("Banner 6", R.drawable.promo_6));
    }

    private void dummyCategory() {
        categories = new ArrayList<>();

        categories.add(new Category("Fashion Wanita", R.drawable.img_post_dummy));
        categories.add(new Category("Fashion Pria", R.drawable.img_post_dummy));
        categories.add(new Category("Handphone", R.drawable.img_post_dummy));
        categories.add(new Category("Komputer", R.drawable.img_post_dummy));
        categories.add(new Category("Elektronik", R.drawable.img_post_dummy));
        categories.add(new Category("Kamera", R.drawable.img_post_dummy));
        categories.add(new Category("Hobi & Koleksi", R.drawable.img_post_dummy));
        categories.add(new Category("Olahraga", R.drawable.img_post_dummy));
        categories.add(new Category("Fashion Anak", R.drawable.img_post_dummy));
        categories.add(new Category("Perlengkapan Bayi", R.drawable.img_post_dummy));
    }

    private void dummyPopular() {
        populars = new ArrayList<>();

        populars.add(new Product("Promo Apple MacBook Pro MPXQ2 2017", 185000000, R.drawable.ic_property));
        populars.add(new Product("Asus A442ur Core i5 8250 NVidia GT930", 85000000, R.drawable.ic_property));
        populars.add(new Product("Asus E202SA-FD112T Core i3 7250 NVidia GT730", 65000000, R.drawable.ic_property));
    }

    @Override
    public void onClick(Object item, int position) {
        Category category = (Category) item;
        navigate(30, category);
    }

    private void navigate(int action, Object data) {
        Intent intent = new Intent(context, SimpleActivity.class);
        intent.putExtra("action", action);
        switch (action) {
            case 30:
                Category category = (Category) data;
                intent.putExtra("data", category);
                break;
        }

        context.startActivity(intent);
    }

    private void popularProduct() {
        //Paging Parameters
        Map<String, String> filters  = new HashMap<>();
        filters.put("page", "1");
        filters.put("sort_by", "id");
        filters.put("order_by", "desc");

        ProductService productService = WebAPI.productService();
        productService.filter(filters).enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                int code = response.code();

                if (code == 200) {
                    populars.addAll(response.body().getData());
                    popularAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void topProduct() {
        ProductService productService = WebAPI.productService();
        productService.top().enqueue(new Callback<Product[]>() {
            @Override
            public void onResponse(Call<Product[]> call, Response<Product[]> response) {
                int code = response.code();

                if (code == 200) {
                    Collections.addAll(populars, response.body());
                    popularAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Product[]> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getCategory() {
        CollectionService listService = WebAPI.collectionService();
        listService.categories(null).enqueue(new Callback<Category[]>() {
            @Override
            public void onResponse(Call<Category[]> call, Response<Category[]> response) {
                int code = response.code();

                switch (code) {
                    case 200:
                        Collections.addAll(categories, response.body());
                        categoryAdapter.notifyDataSetChanged();
                        break;
                }
            }

            @Override
            public void onFailure(Call<Category[]> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}