package xyz.madani.mobileshop.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.callback.OnItemClickListener;
import xyz.madani.mobileshop.model.Product;

/**
 * Created by shaladin on 12/17/17.
 */

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder> {
    private Context context;
    private List<Product> products;
    private OnItemClickListener onItemClickListener;

    public TransactionAdapter(Context context, List<Product> products, OnItemClickListener onItemClickListener) {
        this.context  = context;
        this.products = products;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TransactionViewHolder(LayoutInflater.from(context), parent);
    }

    @Override
    public void onBindViewHolder(TransactionViewHolder holder, final int position) {
        final Product product = products.get(position);
        int[] resourceId = {R.drawable.st_invoice, R.drawable.st_payment, R.drawable.st_delivery, R.drawable.st_shipped,
        R.drawable.st_payment, R.drawable.st_delivery};

        String displayPrice = String.format(Locale.US, "%,.0f", product.getPrice());
        String billId = context.getString(R.string.invoiceId, String.valueOf(10000123+position));
        holder.billId.setText(billId);
        holder.image.setImageResource(resourceId[position]);
        holder.price.setText("Rp"+displayPrice);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            onItemClickListener.onClick(product, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (products == null) ? 0 : products.size();
    }

    public static class TransactionViewHolder extends RecyclerView.ViewHolder {
        TextView billId;
        TextView price;
        TextView status;
        ImageView image;

        public TransactionViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_bill, parent, false));

            billId  = itemView.findViewById(R.id.billId);
            image   = itemView.findViewById(R.id.image);
            price   = itemView.findViewById(R.id.price);
            status  = itemView.findViewById(R.id.status);
        }
    }
}
