package xyz.madani.mobileshop.ui.fragment.product;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.model.Gambar;
import xyz.madani.mobileshop.ui.adapter.ImageAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InspectionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InspectionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Gambar> gambars;

    Spinner quantities;
    TextView name, price;
    ImageView image;
    RecyclerView galleries;

    public InspectionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InspectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InspectionFragment newInstance(String param1, String param2) {
        InspectionFragment fragment = new InspectionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        dummyGambar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inspection, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        quantities = view.findViewById(R.id.quantities);
        quantities.setVisibility(View.GONE);

        LinearLayoutManager lmGallery = new LinearLayoutManager(getContext());

        galleries  = view.findViewById(R.id.gallery);
        galleries.setHasFixedSize(true);
        galleries.setNestedScrollingEnabled(false);
        galleries.setLayoutManager(lmGallery);

        ImageAdapter imageAdapter = new ImageAdapter(getContext(), gambars);
        galleries.setAdapter(imageAdapter);
    }

    private void dummyGambar() {
        gambars = new ArrayList<>();

        gambars.add(new Gambar(R.drawable.iphone_x));
        gambars.add(new Gambar(R.drawable.iphone_x));
    }
}
