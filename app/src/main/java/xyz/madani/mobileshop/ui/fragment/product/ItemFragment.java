package xyz.madani.mobileshop.ui.fragment.product;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.ArrayList;
import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.model.Banner;
import xyz.madani.mobileshop.model.Product;
import xyz.madani.mobileshop.ui.adapter.ProductAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ItemFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context context;
    private List<Banner> banners;
    private List<Product> rekomendasi;

    SliderLayout slider;
    View sRekomendasi;
    RecyclerView rekomendasiView;
    TextView hRekomendasi, mRekomendasi;
    NestedScrollView scrollbar;

    public ItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ItemFragment newInstance(String param1, String param2) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        dummyBanner();
        dummyRekomendasi();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        slider = view.findViewById(R.id.slider);
        sRekomendasi    = view.findViewById(R.id.sRekomendasi);
        rekomendasiView = sRekomendasi.findViewById(R.id.recycler);
        hRekomendasi    = sRekomendasi.findViewById(R.id.title);
        scrollbar       = view.findViewById(R.id.scrollbar);
        scrollbar.setSmoothScrollingEnabled(true);

        for (Banner banner : banners) {
            TextSliderView sliderItem = new TextSliderView(getContext());
            sliderItem.image(banner.getResourceId()).setScaleType(BaseSliderView.ScaleType.Fit);
            sliderItem.bundle(new Bundle());
            sliderItem.getBundle().putString("extra", banner.getName());

            slider.addSlider(sliderItem);
        }

        hRekomendasi.setText(getString(R.string.title_rekomendasi));

        GridLayoutManager gmRekomendasi = new GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false);
        rekomendasiView.setHasFixedSize(true);
        rekomendasiView.setLayoutManager(gmRekomendasi);
        rekomendasiView.setNestedScrollingEnabled(false);

        ProductAdapter rekomendasiAdapter = new ProductAdapter(context, rekomendasi, 10);
        rekomendasiView.setAdapter(rekomendasiAdapter);
    }

    private void dummyBanner() {
        banners = new ArrayList<>();

        banners.add(new Banner("Banner 1", R.drawable.promo_1));
        banners.add(new Banner("Banner 2", R.drawable.promo_2));
        banners.add(new Banner("Banner 3", R.drawable.promo_3));
        banners.add(new Banner("Banner 4", R.drawable.promo_4));
        banners.add(new Banner("Banner 5", R.drawable.promo_5));
        banners.add(new Banner("Banner 6", R.drawable.promo_6));
    }

    private void dummyRekomendasi() {
        rekomendasi = new ArrayList<>();

        rekomendasi.add(new Product("Promo Apple MacBook Pro MPXQ2 2017", 185000000, R.drawable.iphone_x));
        rekomendasi.add(new Product("Asus A442ur Core i5 8250 NVidia GT930", 85000000, R.drawable.iphone_x));
        rekomendasi.add(new Product("Asus E202SA-FD112T Core i3 7250 NVidia GT730", 65000000, R.drawable.iphone_x));
    }

}
