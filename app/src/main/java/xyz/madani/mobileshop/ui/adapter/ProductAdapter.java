package xyz.madani.mobileshop.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.TabActivity;
import xyz.madani.mobileshop.model.Product;

/**
 * Created by shaladin on 12/17/17.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private Context context;
    private List<Product> products;
    private int type;

    public ProductAdapter(Context context, List<Product> products, int type) {
        this.context  = context;
        this.products = products;
        this.type     = type;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductViewHolder(LayoutInflater.from(context), parent, type);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Product product = products.get(position);

        String displayPrice = String.format(Locale.US, "%,.0f", product.getPrice());
        holder.name.setText(product.getName());
        holder.basePrice.setText("Rp"+displayPrice);
//        holder.image.setImageResource(product.getResourceId());
        Picasso.with(context).load(product.getImage()).placeholder(R.drawable.iphone_x)
                .into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detail = new Intent(context, TabActivity.class);
                detail.putExtra("action", 20);
                context.startActivity(detail);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (products == null) ? 0 : products.size();
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView basePrice;
        ImageView image;

        public ProductViewHolder(LayoutInflater inflater, ViewGroup parent, int type) {
            super(inflater.inflate((type == 10) ? R.layout.item_grid_v2:R.layout.item_grid_v3,
                    parent, false));

            name  = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.image);
            basePrice = itemView.findViewById(R.id.price);
        }
    }
}
