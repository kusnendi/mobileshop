package xyz.madani.mobileshop.ui.fragment.product;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.model.KeyValue;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SpecificationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpecificationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context context;
    private List<KeyValue> values;

    ImageView image;
    TextView name, price;
    Spinner quantities;

    public SpecificationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SpecificationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SpecificationFragment newInstance(String param1, String param2) {
        SpecificationFragment fragment = new SpecificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        dummyValues();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_specification, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewGroup specs = view.findViewById(R.id.specs);
        quantities = view.findViewById(R.id.quantities);
        quantities.setVisibility(View.GONE);

        int i = 0;
        for (KeyValue kv : values) {
            View child = LayoutInflater.from(context).inflate(R.layout.item_spesifikasi, specs, false);
            TextView key, val;

            key = child.findViewById(R.id.key);
            val = child.findViewById(R.id.value);

            key.setText(kv.getKey());
            val.setText(kv.getValue());

            if (i%2 == 0) {
                child.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            } else {
                child.setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            specs.addView(child);
            i++;
        }
    }

    private void dummyValues() {
        values = new ArrayList<>();

        values.add(new KeyValue("Ukuran Layar", "5.0 inches"));
        values.add(new KeyValue("Jaringan", "GSM / HSDPA / LTE"));
        values.add(new KeyValue("Sistem Operasi", "Android 6.0 (Marshmallow)"));
        values.add(new KeyValue("Tipe Simcard", "Dual SIM"));
        values.add(new KeyValue("Berat", "100 Gram"));
        values.add(new KeyValue("Merk", "Iphone"));
        values.add(new KeyValue("Megapixels", "8MP & 5MP"));
        values.add(new KeyValue("Fitur Tampilan", "220 ppi"));
        values.add(new KeyValue("Sistem Memori", "2 GB"));
    }

}
