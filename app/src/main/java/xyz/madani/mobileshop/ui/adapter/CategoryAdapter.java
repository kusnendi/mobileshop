package xyz.madani.mobileshop.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.callback.OnItemClickListener;
import xyz.madani.mobileshop.model.Category;

/**
 * Created by shaladin on 12/17/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private Context context;
    private List<Category> categories;
    private OnItemClickListener onItemClickListener;
    private int type;

    public CategoryAdapter(Context context, List<Category> categories) {
        this.context    = context;
        this.categories = categories;
    }

    public CategoryAdapter(Context context, List<Category> categories, int type, OnItemClickListener onItemClickListener) {
        this.context    = context;
        this.categories = categories;
        this.onItemClickListener = onItemClickListener;
        this.type = type;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(context), parent, type);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, final int position) {
        final Category category = categories.get(position);

        holder.title.setText(category.getName());
//        holder.image.setImageResource((category.getResourceId() != 0) ? R.drawable.img_post_dummy : category.getResourceId());

        if (type == 20 && position == 0) holder.separator.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onClick(category, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (categories == null) ? 0 : categories.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;
        View separator;

        public CategoryViewHolder(LayoutInflater inflater, ViewGroup parent, int type) {
            super(inflater.inflate((type == 10) ? R.layout.item_grid_v1 : R.layout.item_simple, parent, false));

            if (type == 10) {
                title = itemView.findViewById(R.id.caption);
                image = itemView.findViewById(R.id.image);
            } else if (type == 20) {
                title = itemView.findViewById(R.id.title);
                image = itemView.findViewById(R.id.icon);
                separator = itemView.findViewById(R.id.separator);
            }
        }
    }
}
