package xyz.madani.mobileshop.ui.fragment.product;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.SimpleActivity;
import xyz.madani.mobileshop.common.EndlessScrollListener;
import xyz.madani.mobileshop.common.Global;
import xyz.madani.mobileshop.http.response.ProductResponse;
import xyz.madani.mobileshop.http.service.ProductService;
import xyz.madani.mobileshop.http.service.WebAPI;
import xyz.madani.mobileshop.model.Category;
import xyz.madani.mobileshop.model.Filter;
import xyz.madani.mobileshop.model.Product;
import xyz.madani.mobileshop.ui.adapter.ProductAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BrowseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BrowseFragment extends Fragment implements SearchView.OnQueryTextListener, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQ_CATEGORY  = 10;
    private static final int REQ_FILTER    = 20;
    private static final int REQ_SORT      = 30;

    private String query;
    private Context context;
    private List<Product> products;
    private ProductAdapter itemAdapter;
    private List<Filter> filters;
    private Map<String, String> filterMap;
    private Category data;
    private int page = 1;

    View view;
    ProgressBar progress;
    TextView mFilter, mCategory, mSort;
    GridLayoutManager itemGridLayout;
    SwipeRefreshLayout refresh;
    RecyclerView productsView;
    SearchView searchView;

    //FIX ME: OnBackPressed SearchView

    public BrowseFragment() {
        // Required empty public constructor
    }

    public static BrowseFragment newInstance(Category category) {
        BrowseFragment fragment = new BrowseFragment();
        Bundle args = new Bundle();
        args.putSerializable("data", category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setHasOptionsMenu(true);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        query    = "";
        data     = (Category) getArguments().getSerializable("data");
        products = new ArrayList<>();
        filterMap= setFilter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_browse, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.view = view;
        mCategory = view.findViewById(R.id.actCategory);
        mFilter   = view.findViewById(R.id.actFilter);
        mSort     = view.findViewById(R.id.actSort);
        progress  = view.findViewById(R.id.progress);

        mCategory.setText(data.getName());
        mCategory.setOnClickListener(this);
        mFilter.setOnClickListener(this);
        mSort.setOnClickListener(this);

        recycleDataView(view, products);
        getData(filterMap);
    }

    private void dummyProducts() {
        products = new ArrayList<>();

        products.add(new Product("Promo Apple MacBook Pro MPXQ2 2017", 185000000, R.drawable.iphone_x));
        products.add(new Product("Asus A442ur Core i5 8250 NVidia GT930", 85000000, R.drawable.iphone_x));
        products.add(new Product("Asus E202SA-FD112T Core i3 7250 NVidia GT730", 65000000, R.drawable.iphone_x));
        products.add(new Product("Promo Apple MacBook Pro MPXQ2 2017", 185000000, R.drawable.iphone_x));
        products.add(new Product("Asus A442ur Core i5 8250 NVidia GT930", 85000000, R.drawable.iphone_x));
        products.add(new Product("Asus E202SA-FD112T Core i3 7250 NVidia GT730", 65000000, R.drawable.iphone_x));
        products.add(new Product("Promo Apple MacBook Pro MPXQ2 2017", 185000000, R.drawable.iphone_x));
        products.add(new Product("Asus A442ur Core i5 8250 NVidia GT930", 85000000, R.drawable.iphone_x));
        products.add(new Product("Asus E202SA-FD112T Core i3 7250 NVidia GT730", 65000000, R.drawable.iphone_x));
        products.add(new Product("Asus E202SA-FD112T Core i3 7250 NVidia GT730", 65000000, R.drawable.iphone_x));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);

        MenuItem optSearch    = menu.findItem(R.id.optSearch);
        searchView = (SearchView) optSearch.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem optNotify = menu.findItem(R.id.optNotify);
        optNotify.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void getData(final Map<String, String> filters) {
        progress.setVisibility(View.VISIBLE);
        ProductService productService = WebAPI.productService();
        productService.filter(filters).enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                int code = response.code();

                if (code == 200) {
                    List<Product> data = response.body().getData();
                    if (data.size() > 0) {
                        page = (response.body().getData().size() > 0) ? response.body().getCurrent() + 1 : 1;

                        if (page-1 == 1 && products.size() > 0) {
                            products.clear();
                        }

                        products.addAll(response.body().getData());
                        itemAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, "Data tidak tersedia...", Toast.LENGTH_SHORT).show();
                    }
                }

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                progress.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //Prepare Filter Params
        page = 1;
        filterMap.put(Filter.page, String.valueOf(page));
        filterMap.put(Filter.keyword, query);

        recycleDataView(view, products);
        getData(filterMap);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String word) {
        if (word.length() == 0) {
            filterMap.put(Filter.page, "1");
            if (filterMap.containsKey(Filter.keyword)) filterMap.remove(Filter.keyword);
            getData(filterMap);
        } else {
            products.clear();
            itemAdapter.notifyDataSetChanged();
        }

        return false;
    }

    private void recycleDataView(View view, List<Product> products) {
        GridLayoutManager itemGridLayout = new GridLayoutManager(context, 2);
        productsView = view.findViewById(R.id.productsView);
        productsView.setHasFixedSize(true);
        productsView.setLayoutManager(itemGridLayout);

        itemAdapter = new ProductAdapter(context, this.products, 20);
        productsView.setAdapter(itemAdapter);
        productsView.addOnScrollListener(pageListener(itemGridLayout));
    }

    private RecyclerView.OnScrollListener pageListener(LinearLayoutManager linearLayoutManager) {
        return new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                System.out.println(String.valueOf(current_page));
                filterMap.put(Filter.page, String.valueOf(current_page));
                getData(filterMap);
            }
        };
    }

    private Map<String, String> setFilter() {
        Map<String, String> map = new HashMap<>();

        map.put(Filter.category, String.valueOf(data.getId()));
        map.put(Filter.sortBy, "created_at");
        map.put(Filter.orderBy, "desc");

        return map;
    }

    private Map<String, String> setFilter(List<Filter> filters) {
        Map<String, String> map = new HashMap<>();

        for (Filter filter : filters) {
            map.put(filter.getParam(), filter.getValue());
        }

        return map;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.actCategory:
                Intent category = new Intent(context, SimpleActivity.class);
                category.putExtra(Global.action, SimpleActivity.LIST_CATEGORY);
                category.putExtra("hasResult", true);
                startActivityForResult(category, SimpleActivity.LIST_CATEGORY);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        SimpleActivity.hasResult = false;
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case SimpleActivity.LIST_CATEGORY:
                    final Category category = (Category) data.getSerializableExtra("result");
                    filterMap.remove(Filter.page);
                    filterMap.put(Filter.category, String.valueOf(category.getId()));

                    products.clear();
                    itemAdapter.notifyDataSetChanged();

                    view.post(new Runnable() {
                        @Override
                        public void run() {
                            mCategory.setText(category.getName());
                        }
                    });

                    recycleDataView(view, products);
                    getData(filterMap);
                    break;
            }
        } else {
            //do other stuff here
        }
    }
}
