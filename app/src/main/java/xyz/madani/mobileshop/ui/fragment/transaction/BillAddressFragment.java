package xyz.madani.mobileshop.ui.fragment.transaction;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.model.Address;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BillAddressFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BillAddressFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Address address;

    public BillAddressFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BillAddressFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BillAddressFragment newInstance(String param1, String param2) {
        BillAddressFragment fragment = new BillAddressFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        address = new Address();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ship_address, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewGroup parent = view.findViewById(R.id.parent);

        if (address.getName() != null) {
            View name = LayoutInflater.from(getContext()).inflate(R.layout.item_address, parent, false);

            TextView keyName = name.findViewById(R.id.key);
            TextView valName = name.findViewById(R.id.value);

            keyName.setText("Nama Pembeli");
            valName.setText(address.getName());
            parent.addView(name);
        }

        if (address.getPhone() != null) {
            View phone = LayoutInflater.from(getContext()).inflate(R.layout.item_address, parent, false);

            TextView keyPhone = phone.findViewById(R.id.key);
            TextView valPhone = phone.findViewById(R.id.value);

            keyPhone.setText("No Telepon");
            valPhone.setText(address.getPhone());
            parent.addView(phone);
        }

        if (address.getStreet() != null) {
            View street = LayoutInflater.from(getContext()).inflate(R.layout.item_address, parent, false);

            TextView keyStreet = street.findViewById(R.id.key);
            TextView valStreet = street.findViewById(R.id.value);

            keyStreet.setText("Alamat");
            valStreet.setText(address.getStreet());
            parent.addView(street);
        }

        if (address.getDistrict() != null) {
            View district = LayoutInflater.from(getContext()).inflate(R.layout.item_address, parent, false);

            TextView keyDistrict = district.findViewById(R.id.key);
            TextView valDistrict = district.findViewById(R.id.value);

            keyDistrict.setText("Kecamatan");
            valDistrict.setText(address.getDistrict());
            parent.addView(district);
        }

        if (address.getCity() != null) {
            View city = LayoutInflater.from(getContext()).inflate(R.layout.item_address, parent, false);

            TextView keyCity = city.findViewById(R.id.key);
            TextView valCity = city.findViewById(R.id.value);

            keyCity.setText("Kota");
            valCity.setText(address.getCity());
            parent.addView(city);
        }

        if (address.getProvince() != null) {
            View province = LayoutInflater.from(getContext()).inflate(R.layout.item_address, parent, false);

            TextView keyProvince = province.findViewById(R.id.key);
            TextView valProvince = province.findViewById(R.id.value);

            keyProvince.setText("Provinsi");
            valProvince.setText(address.getProvince());
            parent.addView(province);
        }

        if (address.getZipCode() != 0) {
            View zip = LayoutInflater.from(getContext()).inflate(R.layout.item_address, parent, false);

            TextView keyZip = zip.findViewById(R.id.key);
            TextView valZip = zip.findViewById(R.id.value);

            keyZip.setText("Kode Pos");
            valZip.setText(String.valueOf(address.getZipCode()));
            parent.addView(zip);
        }
    }
}
