package xyz.madani.mobileshop.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.callback.OnItemClickListener;
import xyz.madani.mobileshop.model.Gambar;
import xyz.madani.mobileshop.model.Menu;

/**
 * Created by developer on 11/12/17.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MenuViewHolder> {
    private Context context;
    private List<Gambar> gambars;
    private OnItemClickListener onItemClickListener;

    public ImageAdapter(Context context, List<Gambar> gambars) {
        this.context    = context;
        this.gambars    = gambars;
//        this.onItemClickListener = listener;
    }

    public ImageAdapter(Context context, List<Gambar> gambars, OnItemClickListener listener) {
        this.context    = context;
        this.gambars    = gambars;
        this.onItemClickListener = listener;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MenuViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public int getItemCount() {
        return (gambars != null) ? gambars.size() : 0;
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, final int position) {
        final Gambar gambar = gambars.get(position);

        holder.image.setImageResource(gambar.getResourceId());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                onItemClickListener.OnItemClickListener(menu, position);
            }
        });
    }

    public static class MenuViewHolder extends RecyclerView.ViewHolder {
        ImageView image;

        public MenuViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_image, parent, false));

            image = itemView.findViewById(R.id.image);
        }
    }
}
