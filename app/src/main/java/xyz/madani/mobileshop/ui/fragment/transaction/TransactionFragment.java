package xyz.madani.mobileshop.ui.fragment.transaction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.TabActivity;
import xyz.madani.mobileshop.callback.OnItemClickListener;
import xyz.madani.mobileshop.model.Product;
import xyz.madani.mobileshop.ui.adapter.TransactionAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link TransactionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TransactionFragment extends Fragment implements OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Product> products;

    RecyclerView lists;
    TransactionAdapter trxAdapter;

    public TransactionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TransactionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TransactionFragment newInstance(String param1, String param2) {
        TransactionFragment fragment = new TransactionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lists = view.findViewById(R.id.lists);

        LinearLayoutManager lmTrx = new LinearLayoutManager(getContext());
        lists.setLayoutManager(lmTrx);
        lists.setHasFixedSize(true);
        trxAdapter = new TransactionAdapter(getContext(), dummyCarts(), this);
        lists.setAdapter(trxAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private List<Product> dummyCarts() {
        List<Product> products = new ArrayList<>();

        for (int i = 1; i<=5; i++) {
            products.add(new Product(getString(R.string.dummy_product), 8500000, R.drawable.iphone_x));
        }

        return products;
    }

    @Override
    public void onClick(Object object, int position) {
        Intent detail = new Intent(getContext(), TabActivity.class);
        detail.putExtra("action", 10);
        startActivity(detail);
    }
}
