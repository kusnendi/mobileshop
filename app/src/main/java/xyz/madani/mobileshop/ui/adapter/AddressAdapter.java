package xyz.madani.mobileshop.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.callback.OnItemClickListener;
import xyz.madani.mobileshop.model.Address;
import xyz.madani.mobileshop.model.Category;

/**
 * Created by shaladin on 12/17/17.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.CategoryViewHolder> {
    private Context context;
    private List<Address> addresses;
    private OnItemClickListener onItemClickListener;
    private int type;

    public AddressAdapter(Context context, List<Address> addresses) {
        this.context    = context;
        this.addresses  = addresses;
    }

    public AddressAdapter(Context context, List<Address> addresses, int type, OnItemClickListener onItemClickListener) {
        this.context    = context;
        this.addresses  = addresses;
        this.onItemClickListener = onItemClickListener;
        this.type = type;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(context), parent, type);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, final int position) {
        final Address address = addresses.get(position);

        if (address.isPrimary()) holder.isPrimary.setVisibility(View.VISIBLE);

        holder.name.setText(address.getName());
        holder.phone.setText(address.getPhone());
        holder.street.setText(address.getStreet());
        holder.city.setText(context.getString(R.string.fmt_city,
                address.getDistrict(), address.getCity()));
        holder.province.setText(context.getString(R.string.fmt_province,
                address.getProvince(), String.valueOf(address.getZipCode())));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemClickListener.onClick(address, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (addresses == null) ? 0 : addresses.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView name, isPrimary, phone, street, city, province;

        public CategoryViewHolder(LayoutInflater inflater, ViewGroup parent, int type) {
            super(inflater.inflate(R.layout.item_address_v2, parent, false));

            isPrimary   = itemView.findViewById(R.id.isPrimary);
            name        = itemView.findViewById(R.id.name);
            phone       = itemView.findViewById(R.id.phone);
            street      = itemView.findViewById(R.id.street);
            city        = itemView.findViewById(R.id.city);
            province    = itemView.findViewById(R.id.province);
        }
    }
}
