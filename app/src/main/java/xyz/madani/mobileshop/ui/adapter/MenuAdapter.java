package xyz.madani.mobileshop.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.model.Menu;

/**
 * Created by developer on 11/12/17.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {
    private Context context;
    private List<Menu> menus;
    private OnItemClickListener onItemClickListener;

    public MenuAdapter(Context context, List<Menu> menus) {
        this.context    = context;
        this.menus      = menus;
//        this.onItemClickListener = listener;
    }

    public MenuAdapter(Context context, List<Menu> menus, OnItemClickListener listener) {
        this.context    = context;
        this.menus      = menus;
        this.onItemClickListener = listener;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MenuViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public int getItemCount() {
        return (menus != null) ? menus.size() : 0;
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, final int position) {
        final Menu menu = menus.get(position);

        if (position == 0) holder.separator.setVisibility(View.GONE);
        holder.title.setText(menu.getName());
        holder.icon.setImageDrawable(context.getResources().getDrawable(menu.getIcon()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                onItemClickListener.OnItemClickListener(menu, position);
            }
        });
    }

    public static class MenuViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView icon;
        View separator;

        public MenuViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_simple, parent, false));

            title = itemView.findViewById(R.id.title);
            icon  = itemView.findViewById(R.id.icon);
            separator = itemView.findViewById(R.id.separator);
        }
    }

    public static interface OnItemClickListener {
        public void OnItemClick(Menu menuItem, int position);
    }
}
