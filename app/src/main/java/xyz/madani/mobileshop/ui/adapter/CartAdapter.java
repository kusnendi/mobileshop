package xyz.madani.mobileshop.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.http.common.Item;
import xyz.madani.mobileshop.http.common.Product;

/**
 * Created by shaladin on 12/17/17.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {
    private Context context;
    private List<Item> items;

    public CartAdapter(Context context, List<Item> items) {
        this.context  = context;
        this.items    = items;
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CartViewHolder(LayoutInflater.from(context), parent);
    }

    @Override
    public void onBindViewHolder(CartViewHolder holder, int position) {
        Item item       = items.get(position);
        Product product = item.getProduct();

        String displayPrice = String.format(Locale.US, "%,.0f", product.getPrice());
        holder.name.setText(product.getName());
        holder.price.setText("Rp"+displayPrice);

        Picasso.with(context).load(product.getImage()).placeholder(R.drawable.iphone_x)
                .into(holder.image);

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent detail = new Intent(context, SimpleActivity.class);
//                context.startActivity(detail);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (items == null) ? 0 : items.size();
    }

    public static class CartViewHolder extends RecyclerView.ViewHolder {
        View root;
        TextView name;
        TextView price;
        ImageView image;

        public CartViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_cart, parent, false));

            root  = itemView.findViewById(R.id.root);
            name  = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.image);
            price = itemView.findViewById(R.id.price);
        }
    }
}
