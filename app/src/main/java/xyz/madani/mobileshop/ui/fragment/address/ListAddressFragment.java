package xyz.madani.mobileshop.ui.fragment.address;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.model.Address;
import xyz.madani.mobileshop.ui.adapter.AddressAdapter;
import xyz.madani.mobileshop.ui.custom.DividerItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListAddressFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListAddressFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context context;
    private List<Address> addresses;

    RecyclerView lists;

    public ListAddressFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListAddressFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListAddressFragment newInstance(String param1, String param2) {
        ListAddressFragment fragment = new ListAddressFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        dummyAddress();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_address, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager listManager = new LinearLayoutManager(context);

        lists = view.findViewById(R.id.lists);
        lists.setLayoutManager(listManager);
        lists.setHasFixedSize(true);

        AddressAdapter adapter = new AddressAdapter(context, addresses);
        lists.setAdapter(adapter);

        RecyclerView.ItemDecoration separator = new DividerItemDecoration(context,
                DividerItemDecoration.VERTICAL_LIST);
        lists.addItemDecoration(separator);
    }

    private void dummyAddress() {
        addresses = new ArrayList<>();
        addresses.add(new Address());
    }

}
