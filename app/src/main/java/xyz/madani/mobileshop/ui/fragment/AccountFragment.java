package xyz.madani.mobileshop.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.model.Menu;
import xyz.madani.mobileshop.ui.adapter.MenuAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Menu> userMenu, otherMenu;

    public AccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountFragment newInstance(String param1, String param2) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        prepareMenu();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager lmUserMenu = new LinearLayoutManager(getContext());
        RecyclerView mnUser = view.findViewById(R.id.userMenu);
        mnUser.setLayoutManager(lmUserMenu);
        mnUser.setHasFixedSize(true);

        MenuAdapter menuAdapter = new MenuAdapter(getContext(), userMenu);
        mnUser.setAdapter(menuAdapter);

        LinearLayoutManager lmOtherMenu = new LinearLayoutManager(getContext());
        RecyclerView mnOther = view.findViewById(R.id.otherMenu);
        mnOther.setLayoutManager(lmOtherMenu);
        mnOther.setHasFixedSize(true);

        MenuAdapter otherAdapter = new MenuAdapter(getContext(), otherMenu);
        mnOther.setAdapter(otherAdapter);
    }

    private void prepareMenu() {
        userMenu = new ArrayList<>();

        userMenu.add(new Menu("Profil", R.drawable.avatar_male));
        userMenu.add(new Menu("Favorite", R.drawable.ic_list_favorite));
        userMenu.add(new Menu("Pengaturan", R.drawable.ic_rocket));

        otherMenu= new ArrayList<>();
        otherMenu.add(new Menu("Q&A", R.drawable.ic_information));
        otherMenu.add(new Menu("Tentang Kami", R.drawable.ic_mobile_store));
        otherMenu.add(new Menu("Hubungi Kami", R.drawable.ic_customer));
    }
}
