package xyz.madani.mobileshop.ui.fragment.checkout;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import xyz.madani.mobileshop.R;
import xyz.madani.mobileshop.SimpleActivity;
import xyz.madani.mobileshop.http.common.Item;
import xyz.madani.mobileshop.model.Product;
import xyz.madani.mobileshop.ui.adapter.CartAdapter;
import xyz.madani.mobileshop.ui.custom.DividerItemDecoration;
import xyz.madani.mobileshop.ui.fragment.address.ListAddressFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShipAddressFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShipAddressFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context context;
    private List<Product> products;
    private List<Item> items;

    TextView otherAddress;
    RecyclerView cartView;
    NestedScrollView scrollbar;
    Button btnPayment;

    public ShipAddressFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShipAddressFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShipAddressFragment newInstance(String param1, String param2) {
        ShipAddressFragment fragment = new ShipAddressFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        items    = new ArrayList<>();
        products = dummyCarts();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ship_address2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        otherAddress = view.findViewById(R.id.otherAddress);
        otherAddress.setOnClickListener(this);

        cartView = view.findViewById(R.id.items);
        scrollbar= view.findViewById(R.id.scrollbar);
        scrollbar.setSmoothScrollingEnabled(true);

        LinearLayoutManager lmCart = new LinearLayoutManager(context);
        cartView.setLayoutManager(lmCart);
        cartView.setHasFixedSize(true);
        cartView.setNestedScrollingEnabled(false);

        CartAdapter cartAdapter = new CartAdapter(context, items);
        cartView.setAdapter(cartAdapter);

        RecyclerView.ItemDecoration separator = new DividerItemDecoration(context,
                DividerItemDecoration.VERTICAL_LIST);
        cartView.addItemDecoration(separator);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollbar.smoothScrollTo(0, 0);
            }
        }, 1000);
    }

    private List<Product> dummyCarts() {
        List<Product> products = new ArrayList<>();

        for (int i = 1; i<=2; i++) {
            products.add(new Product(getString(R.string.dummy_product), 8500000, R.drawable.iphone_x));
        }

        return products;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.otherAddress:
                gotoListAddress();
                break;
        }
    }

    private void gotoListAddress() {
        Intent address = new Intent(context, SimpleActivity.class);
        address.putExtra("action", 60);
        startActivity(address);
    }
}
