package xyz.madani.mobileshop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import xyz.madani.mobileshop.model.Category;
import xyz.madani.mobileshop.ui.custom.LinearLayoutBehaviour;
import xyz.madani.mobileshop.ui.fragment.CategoryFragment;
import xyz.madani.mobileshop.ui.fragment.address.FormAddressFragment;
import xyz.madani.mobileshop.ui.fragment.address.ListAddressFragment;
import xyz.madani.mobileshop.ui.fragment.checkout.PaymentMethodFragment;
import xyz.madani.mobileshop.ui.fragment.checkout.ShipAddressFragment;
import xyz.madani.mobileshop.ui.fragment.product.BrowseFragment;
import xyz.madani.mobileshop.ui.fragment.CartFragment;
import xyz.madani.mobileshop.ui.fragment.product.ItemFragment;

public class SimpleActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int CART          = 10;
    public static final int ITEM_DETAIL   = 20;
    public static final int BROWSE        = 30;
    public static final int CHECKOUT      = 40;
    public static final int PAYMENT       = 50;
    public static final int LIST_ADDRESS  = 60;
    public static final int FORM_ADDRESS  = 70;
    public static final int LIST_CATEGORY = 80;
    public static final String ACTION     = "action";
    public static resultHandler resultHandler;
    public static boolean hasResult;

    private FragmentManager fm;
    private FragmentTransaction ft;
    private ActionBar actionBar;

    View bottomBar;
    TextView lblSubtotal, subtotal;
    FloatingActionButton fab;
    Button btnPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        int action = intent.getIntExtra(ACTION, CART);

        if (intent.hasExtra("hasResult"))
        {
            SimpleActivity.resultHandler = new resultHandler();
            hasResult = intent.getBooleanExtra("hasResult", true);
        }
        else
        {
            hasResult = false;

            fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(this);

            bottomBar = findViewById(R.id.actionbar);
            if (bottomBar != null) {

                subtotal   = (TextView) findViewById(R.id.subtotal);
                lblSubtotal= (TextView) findViewById(R.id.lblSubtotal);
                btnPayment = (Button) findViewById(R.id.btnPayment);

                btnPayment.setOnClickListener(this);

                if (action == PAYMENT) {
                    lblSubtotal.setText(R.string.title_total_tagihan);
                    btnPayment.setText(R.string.btnBayar);
                    btnPayment.setOnClickListener(null);
                }
            }
        }

        navigate(action);
    }

    private void navigate(int eventCode) {
        Fragment fragment = null;

        switch (eventCode) {
            case CART:
                setTitle(getString(R.string.title_keranjang_belanja));
                fragment = new CartFragment();
                break;

            case ITEM_DETAIL:
                setTitle(getString(R.string.title_detail));
                fragment = new ItemFragment();
                break;

            case BROWSE:
                setTitle(getString(R.string.title_browse));
                Category cat = (Category) getIntent().getSerializableExtra("data");
                fragment     = BrowseFragment.newInstance(cat);
                break;

            case CHECKOUT:
                setTitle(getString(R.string.title_checkout));
                fragment = new ShipAddressFragment();
                bottomBar.setVisibility(View.VISIBLE);
                break;

            case PAYMENT:
                setTitle(getString(R.string.title_payment_method));
                fragment = new PaymentMethodFragment();
                bottomBar.setVisibility(View.VISIBLE);
                break;

            case LIST_ADDRESS:
                setTitle(getString(R.string.title_shipping_address_capitalize));
                fab.setVisibility(View.VISIBLE);
                fragment = new ListAddressFragment();
                break;

            case FORM_ADDRESS:
                setTitle(getString(R.string.title_add_shipping_address));
                fragment = new FormAddressFragment();
                break;

            case LIST_CATEGORY:
                setTitle(getString(R.string.title_choose_category));
                fragment = new CategoryFragment();
                break;
        }

        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btnPayment:
                Intent payment = new Intent(this, SimpleActivity.class);
                payment.putExtra(ACTION, PAYMENT);
                startActivity(payment);
                break;

            case R.id.fab:
                Intent formAddress = new Intent(this, SimpleActivity.class);
                formAddress.putExtra(ACTION, FORM_ADDRESS);
                startActivity(formAddress);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public class resultHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            Bundle data  = msg.getData();
            int dataType = data.getInt("dataType");

            Intent intent= new Intent();
            switch (dataType) {
                case LIST_CATEGORY:
                    Category category = (Category) data.getSerializable("result");
                    intent.putExtra("result", category);
                    break;
            }

            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

}
