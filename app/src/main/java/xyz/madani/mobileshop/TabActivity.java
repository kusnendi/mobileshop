package xyz.madani.mobileshop;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import xyz.madani.mobileshop.model.Payment;
import xyz.madani.mobileshop.ui.adapter.PagerAdapter;
import xyz.madani.mobileshop.ui.custom.FrameLayoutBehaviour;
import xyz.madani.mobileshop.ui.fragment.product.InspectionFragment;
import xyz.madani.mobileshop.ui.fragment.product.ItemFragment;
import xyz.madani.mobileshop.ui.fragment.product.SpecificationFragment;
import xyz.madani.mobileshop.ui.fragment.transaction.BillDetailFragment;
import xyz.madani.mobileshop.ui.fragment.transaction.BillStatusFragment;
import xyz.madani.mobileshop.ui.fragment.transaction.BillAddressFragment;

public class TabActivity extends AppCompatActivity implements View.OnClickListener {

    private final String action = "action";
    private int route;

    View bottombar;
    TabLayout tabs;
    ViewPager pager;
    Button btnBuy, btnAdd2Cart;
    ImageButton btnFavorite, btnCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent parent = getIntent();
        route = parent.getIntExtra(action, 10);

        bottombar = findViewById(R.id.bottombar);

        tabs  = (TabLayout) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(setupPagerAdapter());
        tabs.setupWithViewPager(pager);

        if (route == 20) {
            bottombar.setVisibility(View.VISIBLE);

            btnCart     = bottombar.findViewById(R.id.btnCart);
            btnFavorite = bottombar.findViewById(R.id.btnFavorite);
            btnAdd2Cart = bottombar.findViewById(R.id.btnAddToCart);
            btnBuy      = bottombar.findViewById(R.id.btnBuy);

            btnCart.setOnClickListener(this);
            btnFavorite.setOnClickListener(this);
            btnCart.setOnClickListener(this);
            btnBuy.setOnClickListener(this);
        }

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) bottombar.getLayoutParams();
        layoutParams.setBehavior(new FrameLayoutBehaviour());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private PagerAdapter setupPagerAdapter() {
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());

        switch (route) {
            case 10:
                pagerAdapter.addFragment(new BillStatusFragment(), "Status");
                pagerAdapter.addFragment(new BillDetailFragment(), "Detail");
                pagerAdapter.addFragment(new BillAddressFragment(), "Alamat");
                break;
            case 20:
                pagerAdapter.addFragment(new ItemFragment(), "Produk");
                pagerAdapter.addFragment(new InspectionFragment(), "Rincian");
                pagerAdapter.addFragment(new SpecificationFragment(), "Spesifikasi");
                break;
            case 30:
                //do your stuff
                break;
        }

        return pagerAdapter;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btnFavorite:
                addItem2Favorite();
                break;

            case R.id.btnCart:
                gotoCart();
                break;

            case R.id.btnAddToCart:
                addItem2Cart();
                break;

            case R.id.btnBuy:
                gotoShipConfirmation();
                break;
        }
    }

    private void gotoShipConfirmation() {
        Intent confirm = new Intent(this, SimpleActivity.class);
        confirm.putExtra("action", 40);
        startActivity(confirm);
    }

    private void gotoCart() {
        Intent cart = new Intent(this, SimpleActivity.class);
        cart.putExtra("action", 10);
        startActivity(cart);
    }

    private void addItem2Cart() {
        //
    }

    private void addItem2Favorite() {
        //
    }
}
