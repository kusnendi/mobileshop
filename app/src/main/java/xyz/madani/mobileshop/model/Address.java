package xyz.madani.mobileshop.model;

/**
 * Created by shaladin on 12/17/17.
 */

public class Address {
    private String name;
    private String phone;
    private String street;
    private String district;
    private String city;
    private String province;
    private int zipCode;
    private boolean isPrimary;

    public Address() {
        this.name       = "KUSNENDI MUHAMAD";
        this.phone      = "085720858023";
        this.street     = "Grha Adicipta, Jl. Kebon Jeruk Raya No 80";
        this.district   = "Kebon Jeruk";
        this.city       = "Jakarta Barat";
        this.province   = "DKI Jakarta";
        this.zipCode    = 11530;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }
}
