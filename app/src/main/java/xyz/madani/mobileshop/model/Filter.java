package xyz.madani.mobileshop.model;

import java.util.List;

/**
 * Created by shaladin on 12/24/17.
 */

public class Filter {
    public static final String category = "category_id";
    public static final String keyword  = "keyword";
    public static final String sortBy   = "sort_by";
    public static final String orderBy  = "order_by";
    public static final String page     = "page";

    private String param;
    private String value;
    private Category parent;
    private List<Filter> filters;
    private List<Filter> sorts;

    public Filter(String param, String value) {
        this.param = param;
        this.value = value;
    }

    public Filter(Category parent, List<Filter> filters, List<Filter> sorts) {
        this.parent = parent;
        this.filters = filters;
        this.sorts = sorts;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public List<Filter> getSorts() {
        return sorts;
    }

    public void setSorts(List<Filter> sorts) {
        this.sorts = sorts;
    }
}
