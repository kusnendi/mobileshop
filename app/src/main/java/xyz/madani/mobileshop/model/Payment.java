package xyz.madani.mobileshop.model;

import xyz.madani.mobileshop.R;

/**
 * Created by shaladin on 12/17/17.
 */

public class Payment {
    private String name;
    private String imageUrl;
    private String description;
    private int resourceId = R.drawable.ic_radio_unchecked;

    public Payment(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }
}
