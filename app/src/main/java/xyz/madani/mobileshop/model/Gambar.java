package xyz.madani.mobileshop.model;

/**
 * Created by shaladin on 12/25/17.
 */

public class Gambar {
    private String description;
    private String imageUrl;
    private int resourceId;

    public Gambar(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }
}
