package xyz.madani.mobileshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import xyz.madani.mobileshop.R;

/**
 * Created by shaladin on 12/17/17.
 */

public class Product {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("category_ids")
    @Expose
    private String category_id;
    private int resourceId = R.drawable.iphone_x;

    public Product(String name, double price, int resourceId) {
        this.name = name;
        this.price = price;
        this.resourceId = resourceId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }
}
